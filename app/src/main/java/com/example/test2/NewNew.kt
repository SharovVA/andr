package com.example.test2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast

private lateinit var container: LinearLayout

class NewNew : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_new)


        container = findViewById(R.id.container)



    }

    fun addEditTexts(view: View) {
        repeat(3) {
            val editText = EditText(this)
            editText.hint = "Введите текст"
            container.addView(editText)
        }
    }

    fun checkEditTexts(view: View) {
        var allFilled = true
        val editTextData = ArrayList<String>()

        for (i in 0 until container.childCount) {
            val child = container.getChildAt(i)
            if (child is EditText) {
                val text = child.text.toString().trim()
                if (text.isEmpty()) {
                    allFilled = false
                    break
                } else {
                    editTextData.add(text)
                }
            }
        }

        if (allFilled) {
            val intent = Intent(this, Suda::class.java)
            intent.putStringArrayListExtra("editTextData", editTextData)
            startActivity(intent)
        } else {
            Toast.makeText(this, "Не все поля заполнены", Toast.LENGTH_SHORT).show()
        }
    }
}