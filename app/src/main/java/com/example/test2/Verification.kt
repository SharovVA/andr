package com.example.test2

import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.gotrue.OtpType
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.postgrest
import kotlinx.coroutines.launch
import kotlin.random.Random
import kotlinx.serialization.Serializable

class Verification : AppCompatActivity() {

    private lateinit var eeditText1: EditText
    private lateinit var eeditText2: EditText
    private lateinit var eeditText3: EditText
    private lateinit var eeditText4: EditText
    private lateinit var eeditText5: EditText
    private lateinit var eeditText6: EditText
    private lateinit var numbers: EditText

    private lateinit var textt: TextView
    private lateinit var textT: TextView


    private lateinit var forButton: Button
    private lateinit var setButton: Button
    private lateinit var checkButton: Button


    private var randomNumber: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification)

        eeditText1 = findViewById(R.id.editText1)
        eeditText2 = findViewById(R.id.editText2)
        eeditText3 = findViewById(R.id.editText3)
        eeditText4 = findViewById(R.id.editText4)
        eeditText5 = findViewById(R.id.editText5)
        eeditText6 = findViewById(R.id.editText6)

        textt = findViewById(R.id.textView10)
        textT = findViewById(R.id.textView12)

        forButton = findViewById(R.id.button7)
        setButton = findViewById(R.id.button8)
        checkButton = findViewById(R.id.button12)


        numbers = findViewById(R.id.editTextNumber)



        val editTexts = arrayOf(eeditText1, eeditText2, eeditText3, eeditText4, eeditText5, eeditText6)

        //val password = "123456" // Замените это на свои начальные символы

        for (i in 0 until editTexts.size) {
            val editText = editTexts[i]
            editText.addTextChangedListener(object : TextWatcher {
                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s.isNullOrEmpty()) {
                        // Если поле пустое, то установить цвет по умолчанию (белый)
                        editText.setBackgroundColor(Color.WHITE)
                        return
                    }
                    // Проверка вводимых символов на соответствие символам пароля
                    val char = s.toString().toIntOrNull() ?: return
                    editText.setBackgroundColor(Color.BLUE) // Правильный символ - окрашиваем в синий
                        if (i < editTexts.size - 1) {
                            editTexts[i + 1].requestFocus() // Переходим к следующему полю ввода
                    }
                }

                override fun afterTextChanged(s: Editable?) {}
            })
        }
        val handler = Handler()

        setButton.isEnabled = false

        checkButton.setOnClickListener {
            checkData()


        }


        forButton.setOnClickListener {

            autData()

//            randomNumber =123456// Random.nextInt(100000, 999999)
//            // Создаем задачу для включения кнопки обратно через 5 секунд
//            handler.postDelayed({
//                forButton.isEnabled = false
//            }, 5000)//5 сек поменять на 60000
////
//            // Получить текст из всех EditText и собрать числа в одно
//            val number1 = eeditText1.text.toString().toIntOrNull() ?: return@setOnClickListener
//            val number2 = eeditText2.text.toString().toIntOrNull() ?: return@setOnClickListener
//            val number3 = eeditText3.text.toString().toIntOrNull() ?: return@setOnClickListener
//            val number4 = eeditText4.text.toString().toIntOrNull() ?: return@setOnClickListener
//            val number5 = eeditText5.text.toString().toIntOrNull() ?: return@setOnClickListener
//            val number6 = eeditText6.text.toString().toIntOrNull() ?: return@setOnClickListener
//
//            val total = 100000 * number1 + 10000 * number2 + 1000 * number3 + 100 * number4 + 10 * number5 + number6
//
//            textT.text = randomNumber.toString()
//
//            // Проверить с random number и окрасить все поля в красный, если числа не совпали
//            if (total != randomNumber) {
//                textt.text = "Числа не совпали с randomNumber"
//                for (editText in editTexts) {
//                    editText.setBackgroundColor(Color.RED)
//                    forButton.isEnabled = false
//
//                }
//            } else {
//                textt.text = "Числа совпали с randomNumber"
//                forButton.isEnabled = false
//                setButton.isEnabled = true
//            }
        }
    }

    private fun checkData(){
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Auth)
            }

            val numberr = numbers.text.toString()
            // supabase.auth.resendEmail(OtpType.Email.EMAIL_CHANGE, "slava.slavyan.86@mail.ru")
            supabase.auth.resetPasswordForEmail(email = "slava.slavyan.86@mail.ru")
            //supabase.auth.verifyEmailOtp(type = OtpType.Email.EMAIL, email = "example@email.com", token = numberr)

            //val user = supabase.auth.retrieveUserForCurrentSession(updateSession = true)
            //Log.e("supabase", user.toString())

        }

    }


    private fun autData(){
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Auth)
            }

           // supabase.auth.resendEmail(OtpType.Email.EMAIL_CHANGE, "slava.slavyan.86@mail.ru")
            //supabase.auth.resetPasswordForEmail(email = "slava.slavyan.86@mail.ru")

            val result = supabase.auth.resetPasswordForEmail(email = "slava.slavyan.86@mail.ru")
            textt.text = result.toString()
            if (result != null)
            {
                supabase.auth.modifyUser {
                    password = "1234567890"
                }
            }
            else
            {
                textt.text = "ошибка"
            }


        }

    }

    @Serializable
    data class Tokens(
        val id: Int=0,
        val kod: Int=0,
    )

    private fun setData(){
        lifecycleScope.launch {
            val client = getClient()
            val regrow = Tokens(kod = randomNumber)
            client.postgrest["tokens"].insert(regrow)

            Log.e("supabase","успешно передали данные" )
        }
    }

    private fun getData(){
        lifecycleScope.launch {
            val client = getClient()
            val supabaseReponse =  client.postgrest["tokens"].select()
            val data = supabaseReponse.decodeList<reg.User>()
            // Создаем строку, содержащую только имена пользователей
            val namesStringBuilder = StringBuilder()
            data.forEach { user ->
                namesStringBuilder.append(user.name).append("\n")
            }

            // Устанавливаем строку с именами пользователей в TextView
           // textWiew.text = namesStringBuilder.toString()
            Log.e("supabase", data.toString())
        }
    }

    private fun getClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://rowdgzqoplhercnmkmuv.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I",

            ){
            install(Postgrest)
        }
    }
}