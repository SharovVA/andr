package com.example.test2

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter


class SectionsPagerAdapter(fm: FragmentManager?) : FragmentPagerAdapter(fm!!, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FragmentMain() // Замените ThirdFragment() на ваш фрагмент для третьей активити
            1 -> FragmentBord2() // Замените FirstFragment() на ваш фрагмент для первой активити
            2 -> FragmentBord3() // Замените SecondFragment() на ваш фрагмент для второй активити
            3 -> FragmentBord4() // Замените ThirdFragment() на ваш фрагмент для третьей активит
            else -> throw IllegalArgumentException("Invalid position")
        }
    }

    override fun getCount(): Int {
        return 4 // количество ваших страниц
    }

    fun shouldShowText(position: Int): Boolean {
        return position == 0 // Показать текст только на первой странице
    }
}

