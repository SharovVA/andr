package com.example.test2

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity

class SplashAct : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        Handler().postDelayed({ // Переход к Onboard1 после задержки
            val intent = Intent(this@SplashAct, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 2000)
    }
}