package com.example.test2

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.postgrest
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable

class profile : AppCompatActivity() {

    private lateinit var textBalance: TextView
    private lateinit var sendButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        textBalance = findViewById(R.id.textView13)
        sendButton = findViewById(R.id.button13)

        getData()

        sendButton.setOnClickListener {

            val intent = Intent(this, Send::class.java)
            startActivity(intent)

        }
    }

    @Serializable
    data class Balans(
        val id: Int=0,
        val name: String="",
        val balan: Double = 0.0
    )

    private fun getData(){
        lifecycleScope.launch {
            val client = getClient()
            val supabaseReponse =  client.postgrest["balans"].select()
            val data = supabaseReponse.decodeSingle<Balans>()
            // Создаем строку, содержащую только имена пользователей
            //val namesStringBuilder = StringBuilder()
            val balance = data.balan // Получение баланса из строки


            // Устанавливаем строку с именами пользователей в TextView
            textBalance.text = balance.toString()
            Log.e("supabase", data.toString())
        }
    }

    private fun getClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://rowdgzqoplhercnmkmuv.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I",

            ){
            install(Postgrest)
        }
    }

}