package com.example.test2

import android.Manifest
import android.content.pm.PackageManager
import android.location.Geocoder
import android.location.Location
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.TextView
import androidx.compose.ui.graphics.PointMode.Companion.Points
import androidx.core.app.ActivityCompat
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.mapview.MapView
import java.util.Locale

class Send : AppCompatActivity() {

    private lateinit var mapView: MapView
    private lateinit var textGeo: TextView
    private lateinit var textGeo2: TextView

    private lateinit var textGeo21: TextView
    private lateinit var textGeo22: TextView

    private lateinit var geocoder:Geocoder

    var lat: Double? = null
    var lot: Double? = null

    val RequestPermissionCode = 1
    var mLocation: Location? = null
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private val TAG = "Send"

    private lateinit var seButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        MapKitFactory.setApiKey("0cc46aea-7034-4863-84c4-2b943e5ea086")
        MapKitFactory.initialize(this)
        setContentView(R.layout.activity_send)

        mapView = findViewById(R.id.mapview)
        textGeo = findViewById(R.id.textView15)
        textGeo2 = findViewById(R.id.textView16)

        textGeo21 = findViewById(R.id.textView17)
        textGeo22 = findViewById(R.id.textView18)

        seButton = findViewById(R.id.button11)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

        geocoder = Geocoder(this, Locale.getDefault())

        getLastLocation()

        seButton.setOnClickListener {
            getLastLocation()
        }

        mapView.map.move(CameraPosition(Point(lat ?: 0.0, lot ?: 0.0), 11.0f, 0.0f, 0.0f),
                Animation(Animation.Type.SMOOTH, 300f), null)

    }

    private fun getLastLocation()
    {
        if(ActivityCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION) !=PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),10101)
        }

        val LastLocation = fusedLocationProviderClient.lastLocation

        LastLocation.addOnSuccessListener {
            lat =  it.latitude
            lot = it.longitude

            textGeo.text = it.latitude.toString()
            textGeo2.text = it.longitude.toString()

//            Log.d(TAG,"getLastLocation: ${it.latitude}")
//            Log.d(TAG,"getLastLocation: ${it.longitude}")

            var addressList = geocoder.getFromLocation(it.latitude, it.longitude, 1)
            if (addressList != null && addressList.isNotEmpty()) {
                val address: String = addressList[0].getAddressLine(0)
                textGeo21.text = address
            } else {
                // Handle case where no addresses were found
                textGeo21.text = "No address found"
            }
//            textGeo22.text = address[0].locality.toString()


        }
    }

    override fun onStop()
    {
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
        super.onStop()
    }

    override fun  onStart()
    {
        mapView.onStart()
        MapKitFactory.getInstance().onStart()
        super.onStart()
    }



//    private fun getLastLocation() {
//        if (ActivityCompat.checkSelfPermission(
//                this,
//                Manifest.permission.ACCESS_FINE_LOCATION
//            ) != PackageManager.PERMISSION_GRANTED
//        ) {
//            requestPermission()
//        } else {
//            fusedLocationProviderClient.lastLocation
//                .addOnSuccessListener { location: Location? ->
//                    mLocation = location
//                    if (location != null) {
//                        textGeo21.text = location.latitude.toString()
//                        textGeo22.text = location.longitude.toString()
//                    }
//                }
//        }
//    }
//
//    private fun requestPermission() {
//        ActivityCompat.requestPermissions(
//            this,
//            arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
//            RequestPermissionCode
//        )
//        this.recreate()
//    }
}