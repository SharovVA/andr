package com.example.test2

import android.content.Intent
import android.content.pm.LauncherActivityInfo
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.text.method.PasswordTransformationMethod
import android.util.Log
import android.util.Patterns
import android.view.Surface
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.activity.contextaware.withContextAvailable
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.ListItem
import androidx.compose.material3.MaterialTheme
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.mutableStateListOf
import androidx.compose.runtime.remember
import androidx.lifecycle.lifecycleScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient

import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.Google

import io.github.jan.supabase.gotrue.providers.builtin.Email


import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.from
import io.github.jan.supabase.postgrest.postgrest
import io.ktor.websocket.Frame
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlinx.serialization.Serializable
import java.lang.reflect.Modifier

class reg : AppCompatActivity() {

    private lateinit var emailEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var showPasswordButton: Button
    private var isPasswordVisible = false
    private lateinit var confirmPasswordEditText: EditText
    private lateinit var signUpButton: Button
    private lateinit var agreementCheckbox: CheckBox
    private lateinit var textWiew: TextView

    private lateinit var signInGoogleutton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_reg)
        textWiew = findViewById(R.id.textView7)

        getData()

        emailEditText = findViewById(R.id.editTextTextEmailAddress)

        passwordEditText = findViewById(R.id.editTextTextPassword)
        showPasswordButton = findViewById(R.id.button3)

        confirmPasswordEditText = findViewById(R.id.editTextTextPassword2)
        signUpButton = findViewById(R.id.SingUpButton)

        agreementCheckbox = findViewById(R.id.checkbox)

        // Добавляем слушатели для полей ввода пароля и его подтверждения
        passwordEditText.addTextChangedListener(passwordTextWatcher)
        confirmPasswordEditText.addTextChangedListener(confirmPasswordTextWatcher)


        signInGoogleutton = findViewById(R.id.button10)

        // Устанавливаем начальное состояние кнопки
        // signUpButton.isEnabled = false

        // Вызываем проверку email при изменении текста в EditText
                emailEditText.addTextChangedListener(object : TextWatcher {
                    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

                    override fun afterTextChanged(s: Editable?) {
                        checkEmail()
                    }
                })

//        // Добавляем слушатель для кнопки "Зарегистрироваться"
//        signUpButton.setOnClickListener {
//            signUp()
//        }

        // Добавляем слушатель для кнопки "Отобразить пароль"
        showPasswordButton.setOnClickListener {
            togglePasswordVisibility()
        }

        val viewPdfButton: Button = findViewById(R.id.viewPdfButton)
        viewPdfButton.setOnClickListener {
            openPdfViewer()
        }


        signUpButton.setOnClickListener {
            // Проверяем, был ли отмечен чекбокс
            if (passwordEditText.text.toString().trim() == confirmPasswordEditText.text.toString().trim() && agreementCheckbox.isChecked && !passwordEditText.text.toString().trim().isBlank() && !emailEditText.text.toString().trim().isBlank()) {

                //setData()
                autData()
                startActivity(Intent(this@reg, Login::class.java))
            }
            if (!agreementCheckbox.isChecked){
                // Если чекбокс не отмечен, выводим сообщение об ошибке
                agreementCheckbox.error = "Вы должны согласиться с условиями условиями условиями"
            }
            if (passwordEditText.text.toString().trim() != confirmPasswordEditText.text.toString().trim())
            {
                confirmPasswordEditText.error = "Пароли не совпадают"
            }
            if (passwordEditText.text.toString().trim().isBlank()){
                passwordEditText.error = "Пустое поле"
            }
            if (emailEditText.text.toString().trim().isBlank()){
                emailEditText.error = "Пустое поле"
            }
        }

        agreementCheckbox.setOnClickListener {
            // Проверяем, был ли отмечен чекбокс
            if (agreementCheckbox.isChecked) {
                agreementCheckbox.error = null
            }
        }

        signInGoogleutton.setOnClickListener {
            GoogleSingInButton()
        }

    }


    @Serializable
    data class User(
        val id: Int=0,
        val name: String="",
        val family: String ="",
        val email: String=""
    )

    @Serializable
    data class Erg(
        val id: Int=0,
        val email: String="",
        val password: String =""
    )

    private fun autData(){
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Auth)
            }

            val result = supabase.auth.signUpWith(Email) {
                email = emailEditText.text.toString().trim()
                password = passwordEditText.text.toString().trim()
            }
        }

    }


    private fun setData(){
        lifecycleScope.launch {
            val client = getClient()
            val regrow = Erg(email = emailEditText.text.toString().trim(), password = passwordEditText.text.toString().trim())
            client.postgrest["erg"].insert(regrow)

            Log.e("supabase","успешно передали данные" )
        }
    }



    private fun getData(){
        lifecycleScope.launch {
            val client = getClient()
            val supabaseReponse =  client.postgrest["users"].select()
            val data = supabaseReponse.decodeList<User>()
            // Создаем строку, содержащую только имена пользователей
            val namesStringBuilder = StringBuilder()
            data.forEach { user ->
                namesStringBuilder.append(user.name).append("\n")
            }

            // Устанавливаем строку с именами пользователей в TextView
            textWiew.text = namesStringBuilder.toString()
            Log.e("supabase", data.toString())
        }
    }

    private fun getClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://rowdgzqoplhercnmkmuv.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I",

            ){
            install(Postgrest)
        }
    }


    private fun checkEmail() {
        val email: String = emailEditText.text.toString().trim()

        if (TextUtils.isEmpty(email)) {
            // Пустой email
            emailEditText.error = "Пустое поле"
        } else if (!isValidEmail(email)) {
            // Некорректный формат email
            emailEditText.error = "Неправильынй формат"
        } else {
            // Все в порядке
            emailEditText.error = null // Очищаем ошибку, если она была установлена
        }
    }

    private fun isValidEmail(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches() &&
                email.matches("[a-z0-9]+@[a-z0-9]+\\.[a-z]{2,}".toRegex())
    }

    private fun togglePasswordVisibility() {
        if (isPasswordVisible) {
            // Если пароль видим, то скрываем его
            passwordEditText.transformationMethod = PasswordTransformationMethod.getInstance()
            isPasswordVisible = false
            showPasswordButton.text = "Отобразить пароль"
        } else {
            // Если пароль скрыт, то отображаем его
            passwordEditText.transformationMethod = null
            isPasswordVisible = true
            showPasswordButton.text = "Скрыть пароль"
        }

        // Устанавливаем курсор в конец текста
        passwordEditText.setSelection(passwordEditText.text.length)
    }

    private val passwordTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            validatePassword()
        }
    }

    private val confirmPasswordTextWatcher: TextWatcher = object : TextWatcher {
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}

        override fun afterTextChanged(s: Editable?) {
            validatePassword()
        }
    }

    private fun validatePassword() {
        val password: String = passwordEditText.text.toString().trim()
        val confirmPassword: String = confirmPasswordEditText.text.toString().trim()

        if (password != confirmPassword) {
            confirmPasswordEditText.error = "Пароли не совпадают"

        } else {
            confirmPasswordEditText.error = null
        }

    }

//    private fun signUp() {
//        val password: String = passwordEditText.text.toString().trim()
//        val confirmPassword: String = confirmPasswordEditText.text.toString().trim()
//
//        if (password != confirmPassword) {
//            Toast.makeText(this@reg, "Пароли не совпадают", Toast.LENGTH_SHORT).show()
//        } else {
//            // Пароли совпадают, можно выполнять регистрацию или другие действия
//            Toast.makeText(this@reg, "Регистрация успешна", Toast.LENGTH_SHORT).show()
//        }
//    }

    private fun openPdfViewer() {
        val intent = Intent(this, PdfViewerActivity::class.java)
        startActivity(intent)
    }


    private fun GoogleSingInButton(){
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Auth)
            }

            supabase.auth.signInWith(Google)

        }
    }
}