package com.example.test2

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import androidx.viewpager.widget.ViewPager
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.postgrest
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {

    private lateinit var viewPager: ViewPager
    private lateinit var textW: TextView // Предположим, что ваш текст находится в TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getData()
        setContentView(R.layout.activity_main)

        viewPager = findViewById(R.id.viewPager)
        textW = findViewById(R.id.textW) // Инициализируйте ваш TextView


        val pagerAdapter = SectionsPagerAdapter(supportFragmentManager)
        viewPager.adapter = pagerAdapter

        viewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                textW.visibility = View.INVISIBLE
            }

            override fun onPageSelected(position: Int) {
                //updateTextVisibility(position)
            }

            override fun onPageScrollStateChanged(state: Int) {
                // Ничего не делаем при изменении состояния прокрутки
            }
        })

        // Инициализация видимости текста на первой странице
        updateTextVisibility(viewPager.currentItem)

    }

    private fun updateTextVisibility(position: Int) {
        val shouldShowText = (viewPager.adapter as SectionsPagerAdapter).shouldShowText(position)
        textW.visibility = if (shouldShowText) View.VISIBLE else View.INVISIBLE
    }

    private fun getData(){
        lifecycleScope.launch {
            val client = getClient()
            val supabaseReponse =  client.postgrest["users"].select()
            val data = supabaseReponse.decodeList<User>()

            Log.e("supabase", data.toString())
        }
    }
    private fun getClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://rowdgzqoplhercnmkmuv.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I",

        ){
            install(Postgrest)
        }
    }
}
@kotlinx.serialization.Serializable
data class User(
    val id: Int=0,
    val name: String="",
    val family: String ="",
    val email: String=""
)