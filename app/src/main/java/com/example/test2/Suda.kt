package com.example.test2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView

class Suda : AppCompatActivity() {
    private lateinit var textV111: TextView
    private lateinit var textV222: TextView
    private lateinit var textV333: TextView
    private lateinit var textV444: TextView
    private lateinit var textV555: TextView
    private lateinit var textV666: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_suda)

        // Получаем переданные данные из Intent
        val editTextData = intent.getStringArrayListExtra("editTextData")

        // Находим контейнер, в который будем добавлять TextView
        val container = findViewById<LinearLayout>(R.id.container)

        // Добавляем TextView в контейнер
        editTextData?.forEachIndexed { index, data ->
            val textView = TextView(this)
            textView.text = "TextView ${index + 1}: $data"
            container.addView(textView)
        }
    }
}