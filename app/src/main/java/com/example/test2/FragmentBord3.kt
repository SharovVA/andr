package com.example.test2

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment


class FragmentBord3 : Fragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.activity_board3, container, false)
        val button = view.findViewById<Button>(R.id.button2)
        button.setOnClickListener { startNewActivity() }
        return view
    }

    fun startNewActivity() {
        val intent = Intent(activity, Login::class.java)
        startActivity(intent)
    }
}
