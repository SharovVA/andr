package com.example.test2

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.TextView
import androidx.lifecycle.lifecycleScope
import io.github.jan.supabase.SupabaseClient
import io.github.jan.supabase.createSupabaseClient
import io.github.jan.supabase.gotrue.Auth
import io.github.jan.supabase.gotrue.auth
import io.github.jan.supabase.gotrue.providers.builtin.Email
import io.github.jan.supabase.postgrest.Postgrest
import io.github.jan.supabase.postgrest.from
import io.github.jan.supabase.postgrest.postgrest
import io.github.jan.supabase.postgrest.query.Columns
import kotlinx.coroutines.launch
import kotlinx.serialization.Serializable
import java.security.MessageDigest

class Login : AppCompatActivity() {

    private lateinit var emailEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var agreementCheckbox: CheckBox
    private lateinit var loginButton: Button
    private lateinit var forgotButton: Button
    private lateinit var regButton: Button
    private lateinit var textWiew: TextView
    private var  newww: List<Erg>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        emailEditText = findViewById(R.id.editTextTextEmailAddress2)
        passwordEditText = findViewById(R.id.editTextTextPassword3)
        agreementCheckbox = findViewById(R.id.checkBox)
        loginButton = findViewById(R.id.button4)
        forgotButton = findViewById(R.id.button5)
        regButton = findViewById(R.id.button6)

        textWiew = findViewById(R.id.textView9)



        loginButton.setOnClickListener {

            get()
                //сheckData()
                //autData()
           val intent = Intent(this, profile::class.java)
           startActivity(intent)


        }

        forgotButton.setOnClickListener {

            val intent = Intent(this, Verification::class.java)
            startActivity(intent)

        }

        regButton.setOnClickListener {

            val intent = Intent(this, reg::class.java)
            startActivity(intent)

        }

    }
    @Serializable
    data class Erg(
        val id: Int=0,
        val email: String="",
        val password: String =""
    )

    private fun autData(){
        // Очищаем предыдущие данные
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Auth)
            }

            val result = supabase.auth.signInWith(Email) {
                email = emailEditText.text.toString().trim()
                password = passwordEditText.text.toString().trim()
                val hashedPassword = PasswordHasher.hashPassword(password)
            }

            textWiew.text = result.toString()
        }

    }


    object PasswordHasher {
        fun hashPassword(password: String): String {
            val digest = MessageDigest.getInstance("SHA-512")
            val bytes = digest.digest(password.toByteArray(Charsets.UTF_8))
            return bytes.joinToString("") { "%02x".format(it) }
        }
    }

    private fun сheckData(){
        // Очищаем предыдущие данные
        lifecycleScope.launch {
            val client = getClient()
            newww = client.postgrest["erg"].select(columns = Columns.list("email", "password")) {
                filter {
                    and { //when both are true
                        Erg::email eq emailEditText.text.toString().trim()
                        Erg::password eq passwordEditText.text.toString().trim()
                    }
                }

            }.decodeList<Erg>()



            processData()
        }
    }

    private fun processData() {
        // Выполните здесь необходимые операции с переменной newww
        if (!newww.isNullOrEmpty())
        {
            textWiew.text= "балдёж"
        }else{
            textWiew.text= "плохо"
        }
    }

    private fun getData(){
        lifecycleScope.launch {
            val client = getClient()
            val supabaseReponse =  client.postgrest["users"].select()
            val data = supabaseReponse.decodeList<reg.User>()

            val namesStringBuilder = StringBuilder()
            data.forEach { user ->
                namesStringBuilder.append(user.name).append("\n")
            }
            //Log.e("supabase", data.toString())
        }
    }

    private fun getClient(): SupabaseClient {
        return createSupabaseClient(
            supabaseUrl = "https://rowdgzqoplhercnmkmuv.supabase.co",
            supabaseKey = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I",

            ){
            install(Postgrest)
        }
    }

    @Serializable
    data class City(
        val id: Int=0,
        val name: String="",
        val family: String="",
        val email: String="",
    )


    private fun get(){
        // Очищаем предыдущие данные
        lifecycleScope.launch {
            val supabase = createSupabaseClient(
                "https://rowdgzqoplhercnmkmuv.supabase.co",
                "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJzdXBhYmFzZSIsInJlZiI6InJvd2RnenFvcGxoZXJjbm1rbXV2Iiwicm9sZSI6ImFub24iLCJpYXQiOjE3MTAxNDA4MzksImV4cCI6MjAyNTcxNjgzOX0.S68PBt3-yN1H2z5ZKwt9oGnTbYCm_AKU1fGFNE-zV-I"
            ) {
                install(Postgrest)
            }

//            val city = City(name = "The Shire", family = "Sharov", email = "Sharov_vvaaaaaa@mail.ru")
//            supabase.from("users").insert(city)
            val city = supabase.from("users").select(columns = Columns.list("id, name")).decodeSingle<City>()
            textWiew.text= city.toString()

        }

    }

}